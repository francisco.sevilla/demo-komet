
create table demo;

CREATE USER 'demouser'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON * . * TO 'demouser'@'localhost';

create table vendor(
	id smallint unsigned not null auto_increment, 
	vendor_name varchar(50), 
	email varchar(50), 
	comision decimal(5,2), 
	avatar_url varchar(100), 
	actual_comision decimal(10,2), 
	constraint pk_vendor primary key (id)); 

create table sales(
	id smallint unsigned not null auto_increment, 
	sale_date timestamp, 
	vendor_id smallint unsigned, 
	product varchar(100), 
	amount decimal(10,2), 
	constraint pk_sale primary key (id), 
	index (vendor_id), 
	foreign key (vendor_id) 
	references vendor(id) 
	on delete cascade);

insert into vendor(vendor_name, email, comision, avatar_url, actual_comision) 
	values ("Francisco Javier", "mail@cis.co", 5, "https://ui-avatars.com/api/?name=Francisco+Javier", 0),
	("Maria Gabriela", "other@cis.co", 5, "https://ui-avatars.com/api/?name=Maria+Gabriela", 0) ,
	("Ramon Perez", "other-mail@cis.co", 5, "https://ui-avatars.com/api/?name=Ramon+Perez", 0) ,
	("Andrea Ramirez", "another@cis.co", 5, "https://ui-avatars.com/api/?name=Andrea+Ramirez", 0);     
