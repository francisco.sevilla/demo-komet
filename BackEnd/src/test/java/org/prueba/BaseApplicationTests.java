package org.prueba;

import org.junit.jupiter.api.Test;
import org.prueba.controller.VendorController;
import org.prueba.models.VendorModel;
import org.prueba.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.LIST;

@SpringBootTest
class BaseApplicationTests {

	@Autowired
	DemoService service;

	@Autowired
	VendorController controller;

	@Autowired
	ResourceLoader resourceLoader;

	@Test
	void contextLoads() {

	}

	@Test
	void getAllVendorsTest() {
		assertThat(controller.getVendors(0l)).isInstanceOf(List.class);
	}

	@Test
	void getVendorByIdSuccess() {
		assertThat(controller.getVendors(1L)).isInstanceOf(List.class).isNotNull();
	}

	@Test
	void getVendorByIdFail() {
		assertThat(service.getVendorById(10L)).isNull();
	}

	@Test
	void getSalesByVendorSuccess() {
		assertThat(controller.getSales(1L)).isInstanceOf(List.class).isNotNull();
	}

	@Test
	void getSalesByVendorFail() {
		assertThat(controller.getSales(10L)).isInstanceOf(List.class).hasSize(0);
	}

	@Test
	void parseFileSuccess() throws IOException {
		Resource resource = resourceLoader.getResource("classpath:Libro1.csv");
		File file = resource.getFile();
		FileInputStream input = new FileInputStream(file);
		MultipartFile multipartFile = new MockMultipartFile("Libro1.csv", file.getName(), "text/plain", input);
		assertThat(controller.loadFile(multipartFile)).isEqualTo("File loaded successfully");
	}

}
