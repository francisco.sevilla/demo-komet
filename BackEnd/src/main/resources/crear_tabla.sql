/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Admin
 * Created: 05/02/2020
 */
CREATE TABLE `demo`.`camion` 
    (   `id` INT NOT NULL AUTO_INCREMENT , 
        `marca` VARCHAR(20) NOT NULL , 
        `numero_placa` VARCHAR(10) NOT NULL , 
        `ultimo_mantenimiento` DATE NOT NULL , 
        `kilometraje` INT NOT NULL , 
        `licencia` INT NOT NULL , 
    PRIMARY KEY (`id`)) ENGINE = InnoDB;
