/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.prueba.service;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;

import org.prueba.models.SaleCsvModel;
import org.prueba.models.SaleModel;
import org.prueba.models.VendorModel;
import org.prueba.repository.SalesRepository;
import org.prueba.repository.VendorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Admin
 */
@Service
@Transactional
public class DemoService {
    Logger logger = LoggerFactory.getLogger(DemoService.class);

    @Autowired
    private SalesRepository salesRepository;
    @Autowired
    private VendorRepository vendorRepository;

    public VendorModel getVendorById(Long id){
        Optional<VendorModel> result = vendorRepository.findVendor(id);
        if (result.isPresent()){
            return result.get();
        }
        return null;
    }

    @Transactional
    public List<VendorModel> getAllVendors(){
        return vendorRepository.listAll();
    }

    @Transactional
    public List<SaleModel> getSalesByVendor(Long vendor_id){
        return salesRepository.listByVendor(vendor_id);
    }
    
    @Transactional
    public boolean loadFile(MultipartFile file){
        
        List<SaleCsvModel> sales_list = parseCsv(file);
        
        if (sales_list != null && !sales_list.isEmpty()){
            
            logger.info("Records Found: " + sales_list.size());
            int inserts = 0;
            for (SaleCsvModel model : sales_list) {

                vendorRepository.updateVendorActualComission(
                        model.getVendor_id(),
                        model.getAmount());

                salesRepository.insert(model.getSaleDate(), model.getVendor_id(), model.getProduct(), model.getAmount());
                inserts++;
            }
            logger.info("Records Inserted: " + inserts);
            return true;
        }
        
        logger.info("The List is empty!");
        return false;
    }

    public List<SaleCsvModel> parseCsv(MultipartFile file) {

        CsvToBean cb;
        try (Reader reader = new InputStreamReader(file.getInputStream())) {
            cb = new CsvToBeanBuilder(reader)
                    .withType(SaleCsvModel.class)
                    .build();
            logger.info("File builded " + cb.getCapturedExceptions());
        return cb.parse();
        } catch (Exception e) {
            logger.warn(e.getMessage());
            return null;
        }
        
    }
}
