package org.prueba.controller;

import org.prueba.models.SaleModel;
import org.prueba.models.VendorModel;
import org.prueba.service.DemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class VendorController {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private DemoService service;

    @PostMapping("/load")
    @Async
    public String loadFile(@RequestParam("file")  MultipartFile file) {
        
        logger.info("Received file: " + file.getOriginalFilename());
        if (service.loadFile(file)){
            return "File loaded successfully";
        }
        return "File not loaded";
    }

    @GetMapping("/getVendor/{vendor}")
    public List<VendorModel> getVendors(@PathVariable("vendor") Long id){
        if (id == null || id == 0){
            return service.getAllVendors();
        } else {
            List<VendorModel> list = new ArrayList<VendorModel>();
            list.add(service.getVendorById(id));
            return list;
        }
    }

    @GetMapping("/getSales")
    public List<SaleModel> getSales(@RequestParam("vendor") Long id){
        return service.getSalesByVendor(id);
    }
}
