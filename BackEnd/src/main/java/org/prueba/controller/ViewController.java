package org.prueba.controller;

import org.prueba.models.VendorModel;
import org.prueba.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ViewController {

    @RequestMapping("/")
    public ModelAndView getHome(){
        return new ModelAndView("index");
    }

    @RequestMapping("/vendorDetail/{id}")
    public ModelAndView getDetail(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("vendorDetail");
        mav.addObject("id", id);
        return mav;
    }

}
