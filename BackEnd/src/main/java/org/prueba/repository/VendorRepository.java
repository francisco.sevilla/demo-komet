/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.prueba.repository;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.prueba.models.VendorModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Admin
 */
@Repository
public interface VendorRepository extends JpaRepository<VendorModel, Long> {

    @Query(value = "SELECT id, vendor_name, email, comision, avatar_url, actual_comision "
            + "FROM vendor",
            nativeQuery = true)
    public List<VendorModel> listAll();

    @Query(value = "SELECT id, vendor_name, email, comision, avatar_url, actual_comision "
            + "FROM vendor WHERE id=:id",
            nativeQuery = true)
    public Optional<VendorModel> findVendor(@Param("id") Long id);

    @Modifying
    @Query(value = "UPDATE vendor SET actual_comision = ((:sale_amount * comision)/ 100) + actual_comision " +
            "WHERE id=:id",
            nativeQuery = true)
    public int updateVendorActualComission(@Param("id") Long id, @Param("sale_amount") BigDecimal sale_amount);

    @Modifying
    @Query(
            value
            = "insert into vendor (vendor_name, email, comision, avatar_url, actual_comision) "
            + "values (:vendor_name, :email, :comision, :avatar_url, :actual_comision)",
            nativeQuery = true)
    public VendorModel insert(@Param("vendor_name") String vendor_name,
                            @Param("email") String email,
                            @Param("comision") BigDecimal comision,
                            @Param("avatar_url") String avatar_url,
                            @Param("actual_comision") BigDecimal actual_comision);

}
