/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.prueba.repository;

import org.prueba.models.SaleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Admin
 */
@Repository
public interface SalesRepository extends JpaRepository<SaleModel, Long> {

    @Query(value = "SELECT id, sale_date, vendor_id, product, amount "
            + "FROM sales WHERE vendor_id = :vendor_id",
            nativeQuery = true)
    public List<SaleModel> listByVendor( @Param("vendor_id") Long vendor_id);

    @Modifying
    @Query(
            value
            = "insert into sales (sale_date, vendor_id, product, amount) "
            + "values (:sale_date, :vendor_id, :product, :amount)",
            nativeQuery = true)
    public void insert(@Param("sale_date") Date sale_date, @Param("vendor_id") Long vendor_id,
                       @Param("product") String product,
                       @Param("amount") BigDecimal amount);

}
