/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.prueba.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "vendor")
public class VendorModel implements Serializable {
    
    @Id
    private Long id;

    private String vendor_name;

    private String email;

    private BigDecimal comision;

    private String avatar_url;

    private BigDecimal actual_comision;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return vendor_name;
    }

    public void setName(String name) {
        this.vendor_name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getComision() {
        return comision;
    }

    public void setComision(BigDecimal comision) {
        this.comision = comision;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public BigDecimal getActual_comision() {
        return actual_comision;
    }

    public void setActual_comision(BigDecimal actual_comision) {
        this.actual_comision = actual_comision;
    }
}
