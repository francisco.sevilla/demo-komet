/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.prueba.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.opencsv.bean.CsvBindByName;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class SaleCsvModel implements Serializable {

    @CsvBindByName(column = "fecha")
    private String saleDate;
    @CsvBindByName(column = "id_vendedor")
    private Long vendor_id;
    @CsvBindByName(column = "monto")
    private BigDecimal amount;
    @CsvBindByName(column = "producto")
    private String product;

    private SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy");

    public Date getSaleDate() {
        try {
            return new Date(sdf.parse(saleDate).getTime());
        } catch (ParseException ex) {
            Logger.getLogger(SaleCsvModel.class.getName()).log(Level.SEVERE,"No se pudo Parsear la Fecha " + saleDate);
            try {
                return new Date(new SimpleDateFormat("dd-MM-yyyy").parse(saleDate).getTime());
            }catch (ParseException e) {
                Logger.getLogger(SaleCsvModel.class.getName()).log(Level.SEVERE, "Segundo intento Fallido");
                return null;
            }
        }
    }

    public void setSaleDate(Date date) {
        this.saleDate = sdf.format(date);
    }

    public Long getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(Long vendor_id) {
        this.vendor_id = vendor_id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public SimpleDateFormat getSdf() {
        return sdf;
    }

    public void setSdf(SimpleDateFormat sdf) {
        this.sdf = sdf;
    }
}
